---
layout: page
title: About
permalink: /about-me/
---

# Who I am ?

I hack on Free (as in freedom) Software because I know it makes the world a better place. Proprietary software is only meant to exert force over those that use it, while free + open source software gives people the freedom to learn and modify.

I am always interested in giving talks and presentations about the various subjects. if you woruld like me to talk about something at your user group, meetup, (un)conference, or whatever, please feel free to contact me.

## Work Experiences

**PT Boer Technology**<br>
Sept 2019 - Present<br>
Cloud Engineer<br>

**DSITD IPB**<br>
Feb 2019 - Apr 2019<br>
Sysadmin Intern<br>

**Kodetag**<br>
Jul 2018 - Aug 2018<br>
Backend Engineer Intern<br>

## TECHNICAL SKILLS

**Networking**<br>
Routing & Switching<br>

**Programming**<br>
Bash, Python, C, PHP<br>

**Virtualization**<br>
QEMU/KVM, Openstack<br>

**Containerization**<br>
Docker, Kubernetes, Openshift<br>

**Storage**<br>
CEPH, GlusterFS<br>

**CI/CD**<br>
Gitlab CI, Jenkins<br>

**Monitoring & Logging**<br>
Prometheus, Grafana, Elasticsearch<br>

**Automation**<br>
Ansible, Terraform<br>

**Software Defined Networking**<br>
OpenFlow, ONOS<br>

**Operating System**<br>
Ubuntu, Centos<br>

---
layout: post
title:  "Increase scroll back on tmux"
date:   2019-10-26
tags: linux tmux
description: |
  ```bash
  vim .tmux.conf
  set-option -g history-limit 3000
  ```
---

```bash
vim .tmux.conf
set-option -g history-limit 3000
```

**References :** 
- [https://stackoverflow.com/a/18777877](https://stackoverflow.com/a/18777877)
---
layout: post
title : "Konfigurasi Web Server dan OpenSSL"
tags: webserver httpd
description: Catatan tugas kuliah Sistem Operasi Jaringan Komputer. Materi minggu ini tentang setup web server. Web server yang akan di uji coba menggunakan
---

Catatan tugas kuliah Sistem Operasi Jaringan Komputer. Materi minggu ini tentang setup web server. Web server yang akan di uji coba menggunakan httpd. Berikut konfigurasi untuk setup web server httpd : 

```bash
yum install httpd openssl mod_ssl
```

edit file `/etc/httpd/conf/httpd.conf`
```bash
#Listen 12.34.56.78:80
Listen 192.168.3.2:80
ServerAdmin admin@iputra.io
ServerName www.iputra.io:80
```

Buat file `index.html` pada direktori `/var/www/html`
kemudian isi seperti code berikut
```bash
cd /var/www/html
echo "Halo Dunia" > index.html
```

Restart service httpd dengan menggunakan command berikut
```bash
service httpd restart
```

Jika domain `iputra.io` tidak dapat diakses matikan firewall 
atau edit file `/etc/sysconfig/iptables`
```bash
service iptables stop
```

dan tampilan dari browser akan seperti berikut
![tampilan iputra.io](/img/009-iputra-io.png)

jika ingin mengedit file firewal bisa dengan mengisi konfigurasi 
sebagai berikut 
```bash
-A INPUT -p udp -m state --state NEW --dport 53 -j ACCEPT
-A INPUT -p tcp -m state --state NEW --dport 53 -j ACCEPT
-A INPUT -p tcp -m state --state NEW --dport 953 -j ACCEPT
-A INPUT -p udp -m state --state NEW --dport 953 -j ACCEPT
-A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
```

# Konfigurasi OpenSSL
Masuk ke direktori `/etc/pki/tls/certs`
```bash
cd /etc/pki/tls/certs
openssl genrsa -des3 -out server.key 1024
make server.csr
openssl x509 -in server.csr -out server.crt -req -signkey server.key -days 365
cp server.key /etc/pki/tls/private
```

edit file `/etc/httpd/conf.d/ssl.conf`
```bash
Listen 192.168.3.2:443
DocumentRoot "/var/www/html"
ServerName www.iputra.io:443
SSLCertificateFile /etc/pki/tls/certs/server.crt
SSLCertificateKeyFile /etc/pki/tls/private/server.key
```

jika sudah restart service httpd dan jalankan pada browser host 
OS maka akan ada simbol kunci disamping url.
![tampilan https://www.iputra.io](/img/011-https-iputra-io.png)

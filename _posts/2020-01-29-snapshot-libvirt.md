---
layout: post
title:  "Snapshot Libvirt"
date:   2020-01-29
tags: libvirt virtualization kvm
description: |
  ```bash
  # Create Snapshot
  virsh snapshot-create-as --domain ik-kolla-deployer --name "snapshot001" --description "snapshot-description"

  . . .
  ```
---

```bash
# Create Snapshot
virsh snapshot-create-as --domain ik-kolla-deployer --name "snapshot001" --description "snapshot-description"

# Snapshot list
virsh snapshot-list ik-kolla-deployer

# Restore Snapshot
virsh snapshot-revert ik-kolla-deployer --snapshotname snapshot001

# Delete Snapshot
virsh snapshot-delete ik-kolla-deployer snapshot001
```
---
layout: post
title:  "Reset Kubernetes Cluster (kubeadm-reset)"
date:   2019-09-07
tags: kubernetes cloud
description: |
  ```bash
  sudo su
  kubeadm reset
  iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -
  ```
---

```bash
sudo su
kubeadm reset
iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -
```

**References :** 
- [https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-reset/](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-reset/)


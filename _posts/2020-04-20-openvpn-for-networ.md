---
layout: post
title:  "OpenVPN on Network Namespace Just for Watching Netflix"
date:   2020-04-20
tags: NETFLIX OPENVPN NETWORK NAMESPACE
description: This idea come from my research about network namespace on last week and also my ISP block the netflix site. Also, when I'm in need local network . . .
---
This idea come from my research about network namespace on last week and also my ISP block the netflix site. Also, when I'm in need local network without VPN for accessing that doesn't need OpenVPN connection. Thus, I thought to use network namespace for running openvpn connection.

```bash
ip netns add netflix-ns
ip link add netflix-br type bridge
ip link add netflix-veth1 type veth peer name netflix-vpeer1

ip link set netflix-vpeer1 netns netflix-ns
ip link set netflix-veth1 master netflix-br

ip link set dev netflix-br up
ip link set dev netflix-veth1 up
ip netns exec netflix-ns ip link set dev netflix-vpeer1 up
ip netns exec netflix-ns ip link set dev lo up

ip addr add 10.19.98.1/24 dev netflix-br
ip netns exec netflix-ns ip addr add 10.19.98.10/24 dev netflix-vpeer1
ip netns exec netflix-ns ip route add default via 10.19.98.1

iptables -t nat -A POSTROUTING -s 10.19.98.0/24 ! -d 10.19.98.0/24 -o wlp2s0 -j MASQUERADE
iptables -A FORWARD -i netflix-br -o wlp2s0 -j ACCEPT

ip netns exec netflix-ns ping -c 3 8.8.8.8
ip netns exec netflix-ns openvpn --config client.ovpn

# First run
ip netns exec netflix-ns su iputra -c "google-chrome --user-data-dir=netflix-profile --first-run"

# Next Run
ip netns exec netflix-ns su iputra -c "google-chrome --user-data-dir=test --first-run"
```
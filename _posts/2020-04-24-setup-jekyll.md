---
layout: post
title: Setup Jekyll
tags: blog jekyll
description: Setup jekyll demo, buat postingan demo untuk sementara tentang jekyll . . .
---

Setup jekyll demo, buat postingan demo untuk sementara tentang jekyll . . .
```bash
# Install docker
sudo apt install docker.io
usermod -aG docker sudo

# pull ruby image
docker pull ruby:2.5.0

# Run ruby container
mkdir myblog; cd myblog
docker run -dit --name myblog \
  -v $PWD:/srv/jekyll \
  -w /srv/jekyll \
  -p 5000:5000 \
  ruby:2.5.0 bash

# Install jekyll
docker exec -it myblog bash

# Install Jekyll and bundler gems.
gem install jekyll bundler

# Create a new Jekyll site at ./myblog.
jekyll new .

# Build the site and make it available on a local server.
bundle exec jekyll serve --host 0.0.0.0 --port 5000

# Open browser access localhost:5000


# Remove default theme
vi _config.yml
theme: 
```

Quick Start
1. Setup
2. Liquid
3. Front Matter
4. Layouts
5. Includes
6. Data Files
7. Assets
8. Blogging
9. Collections
10. Deployment
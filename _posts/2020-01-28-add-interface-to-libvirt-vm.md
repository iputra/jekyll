---
layout: post
title:  "Add Interface to KVM Guest"
date:   2020-01-28
tags: libvirt virtualization kvm networking
description: |
  ```bash
  # Add Interface
  virsh domiflist ik-node1
  virsh attach-interface --domain ik-node1 --type network \
  . . .
  ```
---

```bash
# Add Interface
virsh domiflist ik-node1
virsh attach-interface --domain ik-node1 --type network \
--source openstackvms --model virtio \
--config --live
virsh domiflist ik-node1

# Check Inside VM
ip a

# Detach Interface
virsh domiflist ik-node1
virsh detach-interface --domain ik-node1 --type network \
--mac 52:53:00:4b:75:6f --configvirsh domiflist ik-node1
```

While detaching, using the "--live" parameter is not recommended as it might affect any existing network activities on the NIC to be removed.
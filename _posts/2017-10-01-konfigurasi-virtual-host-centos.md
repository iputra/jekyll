---
layout: post
title : "Konfigurasi Virtual Host Centos"
tags: webserver httpd virtualhost
description: Catatan tugas kuliah Sistem Operasi Jaringan Komputer. Materi minggu ini lanjutan materi web server. Materi minggu ini yaitu setup . . .
---

Catatan tugas kuliah Sistem Operasi Jaringan Komputer. Materi minggu ini lanjutan materi web server. Materi minggu ini yaitu setup virtual host pada web server httpd. Berikut konfigurasi untuk setup virtual host httpd :

> "Knowing is not enough, we must apply. Willing is not enough, we must do".
> <br>*Johann Wolfgang von Goethe*

Buat direktori baru pada `/var/www/html`
```bash
cd /var/www/html
mkdir -p iputra.io/public_html blog.iputra.io/public_html
```

kemudian pindahkan file yang ada pada `/var/www/html` ke dalam 
`iputra.io/public_html`

<!--
```bash
cp background.png favicon.ico split.css index.html iputra.io/public_html
```
-->

```bash
cp index.html iputra.io/public_html
```

tambahkan file index.html baru ke dalam direktori `/var/www/html/blog.iputra.io/public_html`
```bash
cd /var/www/html/blog.iputra.io/public_html
echo "hello world" > index.html
```
<!-- 
```bash
cd /var/www/html/blog.iputra.io/public_html
curl -O https://iputra.github.io/f/bio/ava.png \
-O https://iputra.github.io/f/bio/ava@2x.png \
-O https://iputra.github.io/f/bio/favicon.ico \
-O https://iputra.github.io/f/bio/style.css \
-O https://iputra.github.io/f/bio/index.html
```
-->

edit file `/etc/httpd/conf/httpd.conf`
```bash

NameVirtualHost bintang:80

<VirtualHost *:80>
    ServerName www.iputra.io
    ServerAdmin admin@iputra.io
    DocumentRoot /var/www/html/iputra.io/public_html
    ErrorLog logs/iputra.io-error_log
    CustomLog logs/iputra.io-access_log common
</VirtualHost>

<VirtualHost *:80>
    ServerName blog.iputra.io
    ServerAdmin admin@iputra.io
    DocumentRoot /var/www/html/blog.iputra.io/public_html
    ErrorLog logs/iputra.io-error_log
    CustomLog logs/iputra.io-access_log common
</VirtualHost>
```

sekarang edit untuk https nya pada file `/etc/httpd/conf.d/ssl.conf`
```bash
DocumentRoot "/var/www/html/iputra.io/public_html"
ServerName www.iputra.io:443
```

jika sudah restart service httpd

maka jika diakses `iputra.io` akan tampil seperti ini

![Tampilan iputra.io](/img/009-iputra-io.png)

dan jika diakses `blog.iputra.io` maka akan seperti ini
![Tampilan blog.iputra.io](/img/010-blog-iputra-io.png)

**References :**

- [https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-centos-6](https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-centos-6)
- [https://www.digicert.com/ssl-support/apache-multiple-ssl-certificates-using-sni.htm](https://www.digicert.com/ssl-support/apache-multiple-ssl-certificates-using-sni.htm)

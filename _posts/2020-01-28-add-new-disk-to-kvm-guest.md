---
layout: post
title:  "Add New Disk to KVM Guest"
date:   2020-01-28
tags: libvirt virtualization kvm storage
description: |
  ```bash
  # Create disk
  qemu-img create -f qcow2 ik-node1-1.qcow2 10G
  qemu-img create -f qcow2 ik-node1-2.qcow2 10G
  . . .
  ```
---

```bash
# Create disk
qemu-img create -f qcow2 ik-node1-1.qcow2 10G
qemu-img create -f qcow2 ik-node1-2.qcow2 10G
qemu-img create -f qcow2 ik-node1-3.qcow2 10G

qemu-img create -f qcow2 ik-node2-1.qcow2 10G
qemu-img create -f qcow2 ik-node2-2.qcow2 10G
qemu-img create -f qcow2 ik-node2-3.qcow2 10G

qemu-img create -f qcow2 ik-node3-1.qcow2 10G
qemu-img create -f qcow2 ik-node3-2.qcow2 10G
qemu-img create -f qcow2 ik-node3-3.qcow2 10G

# Attach disk
virsh attach-disk ik-node1 --source /data/vms/ik-node1-1.qcow2 --target vdb --config --driver qemu --subdriver qcow2 --targetbus virtio
virsh attach-disk ik-node1 --source /data/vms/ik-node1-2.qcow2 --target vdc --config --driver qemu --subdriver qcow2 --targetbus virtio
virsh attach-disk ik-node1 --source /data/vms/ik-node1-3.qcow2 --target vdd --config --driver qemu --subdriver qcow2 --targetbus virtio

virsh attach-disk ik-node2 --source /data/vms/ik-node2-1.qcow2 --target vdb --config --driver qemu --subdriver qcow2 --targetbus virtio
virsh attach-disk ik-node2 --source /data/vms/ik-node2-2.qcow2 --target vdc --config --driver qemu --subdriver qcow2 --targetbus virtio
virsh attach-disk ik-node2 --source /data/vms/ik-node2-3.qcow2 --target vdd --config --driver qemu --subdriver qcow2 --targetbus virtio

virsh attach-disk ik-node3 --source /data/vms/ik-node3-1.qcow2 --target vdb --config --driver qemu --subdriver qcow2 --targetbus virtio
virsh attach-disk ik-node3 --source /data/vms/ik-node3-2.qcow2 --target vdc --config --driver qemu --subdriver qcow2 --targetbus virtio
virsh attach-disk ik-node3 --source /data/vms/ik-node3-3.qcow2 --target vdd --config --driver qemu --subdriver qcow2 --targetbus virtio
```
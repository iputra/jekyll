---
layout: post
title: Plan 9 Distributed OS run on QEMU
tags: plan9 qemu virtualization operatingsystem
date: 2020-03-30
description: Under Plan 9, UNIX's everything is a file metaphor is extended via a pervasive network-centric filesystem, and the cursor-addressed, terminal-based I/O at . . .
---

> Under Plan 9, UNIX's everything is a file metaphor is extended via a pervasive network-centric filesystem, and the cursor-addressed, terminal-based I/O at the heart of UNIX-like operating systems is replaced by a windowing system and graphical user interface without cursor addressing, although rc, the Plan 9 shell, is text-based.
> 
> *Wikipedia*

```bash
wget https://9p.io/plan9/download/plan9.iso.bz2
bunzip2 plan9.iso.bz2

qemu-img create -f qcow2 plan9-disk.img 10G

qemu-system-i386 -hda plan9-disk.img -cdrom plan9.iso -boot d -usb -device -usb-tablet
```

**References :**
- [Plan 9 from Bell Labs](https://9p.io/plan9/)
- [Installing Plan 9 on Qemu](https://9p.io/wiki/plan9/Installing_Plan_9_on_Qemu/index.html)

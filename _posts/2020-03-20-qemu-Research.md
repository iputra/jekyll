---
layout: post
title: QEMU Research
date: 2020-03-20
tags: qemu virtualization kvm
description: by default instance that qemu create has internet access. But the instance cannot be using ping to proof that internet access, to proof that you can use curl or by updating . . .
---

by default instance that qemu create has internet access. But the instance cannot be using ping to proof that internet access, to proof that you can use curl or by updating your repository or just using browser as usual.

```bash
wget http://download.cirros-cloud.net/0.5.1/cirros-0.5.1-x86_64-disk.img

# Simple running image
qemu-system-x86_64 cirros-0.5.1-x86_64-disk.img

# Add 2 core processor
qemu-system-x86_64 **-smp 2** cirros-0.5.1-x86_64-disk.img

# Add 512 MB memory
qemu-system-x86_64 -smp 2 **-m 512** cirros-0.5.1-x86_64-disk.img

# Create disk
qemu-img create -f qcow2 ubuntu-disk.img 10G

# Bootable from livecd
# boot menu value : 
# a,b = floopy disk 1, floopy disk2
# c = hdd
# d = cdrom
# n-p = ethernet from 1 - 4
# default = from hdd
qemu-system-x86_64 -smp 2 -m 2048 -hda ubuntu-disk.img \
    -cdrom ubuntu-mate-18.04.4-desktop-amd64.iso \
    **-boot order=d**

# Enable KVM
qemu-system-x86_64 -smp 2 -m 2048 -hda ubuntu-disk.img \
    -cdrom ubuntu-mate-18.04.4-desktop-amd64.iso \
    -boot menu=d **-enable-kvm**

# Enable mouse and keyboard
qemu-system-x86_64 -smp 2 -m 2048 -hda ubuntu-disk.img \
    -cdrom ubuntu-mate-18.04.4-desktop-amd64.iso \
    -boot menu=d -enable-kvm **-usb -device usb-tablet**

# Use other VGA
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2 \
  -boot order=c,menu=off \
  -enable-kvm \
  **-vga virtio**

# Full Screen
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2 \
  -boot order=c,menu=off \
  -enable-kvm \
  -vga virtio \
  **-full-screen

# Disk with virtio**
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2**,if=virtio** \
  -boot order=c,menu=off \
  -enable-kvm \
  -vga virtio

# Add network tap interface connect to bridge with internet access
## Requirements
sysctl net.ipv4.ip_forward
sysctl -w net.ipv4.ip_forward=1

IP tuntap add dev tap0 mode tap
ip link set dev tap0 up

ip link add br0 type bridge
ip link set br0 up
ip addr add 10.0.1.1/24 dev br0

ip link set tap0 master br0

iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -A FORWARD -i blok-br0 -o eth0 -j ACCEPT

## Legacy QEMU
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2,if=virtio \
  -boot order=c,menu=off \
  -enable-kvm \
  -vga virtio \
  **-device virtio-net-pci,netdev=network0 -netdev tap,id=network0,ifname=tap0,script=no,downscript=no**

## Newer QEMU
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2,if=virtio \
  -boot order=c,menu=off \
  -enable-kvm \
  -vga virtio \
  **-nic tap,ifname=tap0,script=no,downscript=no,model=virtio-net-pci**

# Network custom macaddr
## Legacy QEMU
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2,if=virtio \
  -boot order=c,menu=off \
  -enable-kvm \
  -vga virtio \
  ****-device virtio-net-pci,netdev=network0**,mac=52:54:98:76:54:32** -netdev tap,id=network0,ifname=tap0,script=no,downscript=no
****
## Newer QEMU
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2,if=virtio \
  -boot order=c,menu=off \
  -enable-kvm \
  -vga virtio \
  -nic tap,ifname=tap0,script=no,downscript=no,model=virtio-net-pci**,mac=52:54:98:76:54:32**

## Check network model
qemu-system-x86_64 -nic model=help

# Nographic just use serial console
## Configure grub first

vim /etc/default/grub
...
GRUB_CMDLINE_LINUX="console=tty1 console=ttyS0,115200"
GRUB_TERMINAL="console serial"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"
...

grub-mkconfig -o /boot/grub/grub.cfg
reboot

## boot with nographic
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2,if=virtio \
  -boot order=c,menu=off \
  -enable-kvm \
  -nic tap,ifname=tap0,script=no,downscript=no,model=virtio-net-pci**,**mac=02:06:19:98:00:00 **\
  -nographic -serial mon:stdio**

## or using curses
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=template-centos7.7.qcow2,format=qcow2,if=virtio \
  -boot order=c,menu=off \
  -enable-kvm \
  -nic tap,ifname=tap0,script=no,downscript=no,model=virtio-net-pci**,**mac=02:06:19:98:00:00 **\
  -curses**

# Boot ubuntu image with cloud-init
sudo apt install -y cloud-image-utils
mkdir ~/qemu; cd ~/qemu

vim cloud_init.cfg
...
#cloud-config
hostname: node0
users:
  - name: ubuntu
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: users, admin
    home: /home/ubuntu
    shell: /bin/bash
    lock_passwd: false
    ssh-authorized-keys:
      - <public-key>
ssh_pwauth: False
chpasswd:
  list: |
    ubuntu:yasmin88
  expire: False
...

vim net_conf.cfg
version: 2
ethernets:
  ens3:
     dhcp4: false
     addresses: [ 10.0.0.100/24 ]
     gateway4: 10.0.0.1
     nameservers:
       addresses: [ 10.0.0.1,8.8.8.8 ]

cloud-localds -v --network-config=net_conf.cfg node0-seed.qcow2 cloud_init.cfg

qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=node0.img,format=qcow2,if=virtio \
  **-drive file=node0-seed.qcow2,format=raw,if=virtio \**
  -boot order=c,menu=off \
  -enable-kvm \
  ****-device virtio-net-pci,netdev=network0,mac=02:06:19:98:00:00 ****-netdev tap,id=network0,ifname=tap0,script=no,downscript=no \
  ****-nographic

# Boot centos image with cloud-init
vim meta-data
instance-id: node1
local-hostname: node1
network-interfaces: |
  iface eth0 inet static
  address 10.0.0.100
  network 10.0.0.0
  netmask 255.255.255.0
  broadcast 10.0.0.255
  gateway 10.0.0.1
  onboot yes
  name eth0

vim user-data
#cloud-config
users:
  - name: centos
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: users, admin
    home: /home/ubuntu
    shell: /bin/bash
    lock_passwd: false
    ssh-authorized-keys:
      - <public-key>
ssh_pwauth: False
chpasswd:
  list: |
    root:linux
    centos:yasmin88
  expire: False

genisoimage -output cloud-init.iso -volid cidata -joliet -rock user-data meta-data
qemu-system-x86_64 -smp 2 -m 2048 \
  -drive file=node0.img,format=qcow2,if=virtio \
  **-drive file=cloud-init.iso,format=raw,if=virtio \**
  -boot order=c,menu=off \
  -enable-kvm \
  ****-device virtio-net-pci,netdev=network0,mac=02:06:19:98:00:00 ****-netdev tap,id=network0,ifname=tap0,script=no,downscript=no \
  ****-nographic
```

```bash
# resize qemu-img
qemu-img resize /data/vms/ik-vz7-disk0.qcow2 +24G

# Mount qemu image
sudo apt install -y qemu-utils
sudo modprobe nbd max_part=8
qemu-nbd -c /dev/nbd0 --read-only node0.img
fdisk -l /dev/nbd0
mount -o ro /dev/nbd0p1 /mnt

# Umount qemu image
umount /mnt/
qemu-nbd --disconnect /dev/nbd0
rmmod nbd
```

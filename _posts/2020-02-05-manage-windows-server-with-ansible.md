---
layout: post
title: "Manage Windows Server 2012 With Ansible"
date: 2020-02-05
tags: ansible windowsserver
description: |
  ```bash
  - Create User
  - Add this code
  winrm set winrm/config/service @{AllowUnencrypted="true"}
  . . .
  ```
---
```bash
- Create User
- Add this code
winrm set winrm/config/service @{AllowUnencrypted="true"}
winrm set winrm/config/service/auth @{Basic="true"}
- Add firewall rule icmp and 5985

on the control node
pip install pywinrm

vi inventory
[win]
10.10.10.100

[win:vars]
ansible_user=Administrator
ansible_password="secret@1337"
ansible_connection=winrm
ansible_winrm_transport=basic
ansible_winrm_port=5985

ansible -m win_ping -i inventory win
```
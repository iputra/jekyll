---
layout: post
title:  "Keep SSH Connection Alive"
date:   2020-01-30
tags: linux ssh
description: |
  ```bash
  vi .ssh/config
  Host *
  ServerAliveInterval 120
  . . .
  ```
---

```bash
vi .ssh/config

Host *            
    ServerAliveInterval 120
    ServerAliveCountMax 720
```

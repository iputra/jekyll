---
layout: post
title:  "Enable Mouse on ViM"
date:   2019-10-22
tags: linux vim
description: |
  ```bash
  :set mouse=a
  ```
---

```bash
:set mouse=a
```

**References :** 
- [https://vim.fandom.com/wiki/Using_the_mouse_for_Vim_in_an_xterm](https://vim.fandom.com/wiki/Using_the_mouse_for_Vim_in_an_xterm)
---
layout: post
title: My research about libvirt
tags: libvirt kvm virtualization
date: 2020-03-12
description: libvirt is an abstraction for virtualization. It abstract some hypervisor like qemu/kvm, xen, hyperv, etc. This page contain my research about libvirt exactly about my observation . . .
---

libvirt is an abstraction for virtualization. It abstract some hypervisor like qemu/kvm, xen, hyperv, etc. This page contain my research about libvirt exactly about my observation. Why I do research about libvirt ? because, it start from my curiosity about how virt-manager work, then guide me to the libvirt, qemu, kvm, etc. virt-manager is part of libvirt, it is gui client tool that can we use to create or manage virtual machine, storage pool, virtual network, and doing any cool stuff. So below this is my observation of libvirt : 

- libvirt use network bridge to create their default virtual network (read more: iproute2 and brctl)
- libvirt use dnsmasq as dhcp-server for their virtual network

```bash
$ ls /var/lib/libvirt/dnsmasq
default.addnhosts    net-10.40.40.addnhosts    virbr0.macs
default.conf         net-10.40.40.conf         virbr0.status
default.hostsfile    net-10.40.40.hostsfile    virbr1.macs

$ cat /var/lib/libvirt/dnsmasq/net-10.40.40.conf
strict-order
user=libvirt-dnsmasq
domain=net-10.40.40
expand-hosts
pid-file=/var/run/libvirt/network/net-10.40.40.pid
except-interface=lo
bind-dynamic
interface=virbr1
addn-hosts=/var/lib/libvirt/dnsmasq/net-10.40.40.addnhosts

$ virsh net-list
Name                 State      Autostart     Persistent
----------------------------------------------------------
 default              active     yes           yes
 net-10.40.40         active     yes           yes
```

- libvirt store the id in memory so if you restart the libvirtd service the id will start from beginning. Below snippet is prove about that. From that snippet we know that libvirt store the id in memory as long as their libvirtd service is running.

```bash
$ virsh start ik-master0
Domain ik-master0 started

$ virsh start ik-master1
Domain ik-master1 started

$ virsh list
Id    Name                           State
----------------------------------------------------
 1     master0                       running
 2     master1                       running

# restart libvirtd service, start domain ik-master1 then ik-master0
# ik-master1 will get id=1, then ik-master0 will get id=2
$ systemctl restart libvirtd

$ virsh start ik-master1
Domain ik-master1 started

$ virsh start ik-master0
Domain ik-master0 started

$ virsh list
Id    Name                           State
----------------------------------------------------
 1     master1                       running
 2     master0                       running
```

- libvirt store a domain in a configuration file which formatted to xml. If we delete or move the domain configuration file from the directory also we restart the libvirtd then it will make the domain cannot appear from the virsh or we can say it will removed.

```bash
# libvirt store domain in /etc/libvirt/qemu
$ ls /etc/libvirt/qemu
master0.xml    master1.xml    master2.xml

$ virsh list --all
Id    Name                           State
----------------------------------------------------
 1     master1                       running
 2     master0                       running
 -     master2                       shut off

$ mv /etc/libvirt/qemu/master2.xml /tmp/master2.xml
$ systemctl restart libvirtd

$ virsh list --all
Id    Name                           State
----------------------------------------------------
 1     master1                       running
 2     master0                       running
```

- If we restart libvirtd service and no error in our configuration, domain and other object still running normally.

```bash
$ virsh list --all
Id    Name                           State
----------------------------------------------------
 1     master1                       running
 2     master0                       running

$ systemctl restart libvirtd

$ virsh list --all
Id    Name                           State
----------------------------------------------------
 1     master1                       running
 2     master0                       running
```
